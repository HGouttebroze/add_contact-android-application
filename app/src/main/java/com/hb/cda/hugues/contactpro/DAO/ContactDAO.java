package com.hb.cda.hugues.contactpro.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.hb.cda.hugues.contactpro.db.ContactDBHelper;
import com.hb.cda.hugues.contactpro.pojos.Contact;

import java.util.ArrayList;
import java.util.List;

public class ContactDAO extends DAO{

    public ContactDAO(Context context) {
        super(new ContactDBHelper(context));
    }

    public Contact find(int id) {
        Contact contact = null;

        // open data base
        open();

        Cursor cursor = db.rawQuery("select * from " + ContactDBHelper.CONTACT_TABLE_NAME +
                " where " + ContactDBHelper.CONTACT_KEY + " = ?",
                new String[] { String.valueOf(id) });

        // put cursor on first record
        if (cursor != null && cursor.moveToFirst()) {
            contact = new Contact();
            contact.setId(cursor.getLong(ContactDBHelper.CONTACT_KEY_COLUMN_INDEX));
            contact.setNom(cursor.getString(ContactDBHelper.CONTACT_NOM_COLUMN_INDEX));
            contact.setPrenom(cursor.getString(ContactDBHelper.CONTACT_PRENOM_COLUMN_INDEX));
            contact.setAdresse(cursor.getString(ContactDBHelper.CONTACT_ADRESSE_COLUMN_INDEX));
            contact.setSociete(cursor.getString(ContactDBHelper.CONTACT_SOCIETE_COLUMN_INDEX));
            contact.setTel(cursor.getString(ContactDBHelper.CONTACT_TEL_COLUMN_INDEX));
            contact.setAdresseMail(cursor.getString(ContactDBHelper.CONTACT_ADRESSEMAIL_COLUMN_INDEX));
            contact.setSiteWeb(cursor.getString(ContactDBHelper.CONTACT_SITEWEB_COLUMN_INDEX));
            contact.setSecteur(cursor.getString(ContactDBHelper.CONTACT_SECTEUR_COLUMN_INDEX));
            //to do: favori in INTEGER: contact.setFavori();

            // close cursor
            cursor.close();
        }

        // close data base
        close();

        return contact;
    }

    public List<Contact> list() {

        // objects list variable
        List<Contact> contacts = new ArrayList<>();

        // open data base
        open();

        // execute query & check a cursor with data
        Cursor cursor = db.rawQuery("select * from " + ContactDBHelper.CONTACT_TABLE_NAME, null);

        // put cursor on first record
        if (cursor != null && cursor.moveToFirst()) {
            // while until cursor's not arrived to last record
            while(!cursor.isAfterLast()) {
                Contact contact = new Contact();
                contact.setId(cursor.getLong(ContactDBHelper.CONTACT_KEY_COLUMN_INDEX));
                contact.setNom(cursor.getString(ContactDBHelper.CONTACT_NOM_COLUMN_INDEX));
                contact.setPrenom(cursor.getString(ContactDBHelper.CONTACT_PRENOM_COLUMN_INDEX));
                contact.setAdresse(cursor.getString(ContactDBHelper.CONTACT_ADRESSE_COLUMN_INDEX));
                contact.setSociete(cursor.getString(ContactDBHelper.CONTACT_SOCIETE_COLUMN_INDEX));
                contact.setTel(cursor.getString(ContactDBHelper.CONTACT_TEL_COLUMN_INDEX));
                contact.setAdresseMail(cursor.getString(ContactDBHelper.CONTACT_ADRESSEMAIL_COLUMN_INDEX));
                contact.setSiteWeb(cursor.getString(ContactDBHelper.CONTACT_SITEWEB_COLUMN_INDEX));
                contact.setSecteur(cursor.getString(ContactDBHelper.CONTACT_SECTEUR_COLUMN_INDEX));
                //to do: favori in INTEGER: contact.setFavori();


                // add contact created on list
                contacts.add(contact);

                // move next record
                cursor.moveToNext();
            }

            // close cursor
            cursor.close();
        }

        // close data base
        close();

        return contacts;
    }

    public void add(Contact contact) {
        // open data base
        open();

        ContentValues values = new ContentValues();

        values.put(ContactDBHelper.CONTACT_NOM, contact.getNom());
        values.put(ContactDBHelper.CONTACT_PRENOM, contact.getPrenom());
        values.put(ContactDBHelper.CONTACT_SOCIETE, contact.getSociete());
        values.put(ContactDBHelper.CONTACT_ADRESSE, contact.getAdresse());
        values.put(ContactDBHelper.CONTACT_TEL, contact.getTel());
        values.put(ContactDBHelper.CONTACT_ADRESSEMAIL, contact.getAdresseMail());
        values.put(ContactDBHelper.CONTACT_SITEWEB, contact.getSiteWeb());
        values.put(ContactDBHelper.CONTACT_SECTEUR, contact.getSecteur());
        //to do: favori in INTEGER: values.put(ContactDBHelper, contact).......;

        //  make a data input & check generated id
        long id = db.insert(ContactDBHelper.CONTACT_TABLE_NAME, null, values);

        // update object's id
        contact.setId(id);

        // close data base
        close();
    }

    public void update(Contact contact) {
        // open data base
        open();

        ContentValues values = new ContentValues();

        values.put(ContactDBHelper.CONTACT_NOM, contact.getNom());
        values.put(ContactDBHelper.CONTACT_PRENOM, contact.getPrenom());
        values.put(ContactDBHelper.CONTACT_SOCIETE, contact.getSociete());
        values.put(ContactDBHelper.CONTACT_ADRESSE, contact.getAdresse());
        values.put(ContactDBHelper.CONTACT_TEL, contact.getTel());
        values.put(ContactDBHelper.CONTACT_ADRESSEMAIL, contact.getAdresseMail());
        values.put(ContactDBHelper.CONTACT_SITEWEB, contact.getSiteWeb());
        values.put(ContactDBHelper.CONTACT_SECTEUR, contact.getSecteur());
        //to do: favori in INTEGER: values.put(ContactDBHelper, contact);

        // execute update with: `where id = ?` query
        db.update(ContactDBHelper.CONTACT_TABLE_NAME, values, ContactDBHelper.CONTACT_KEY + " = ?",
                new String[] { String.valueOf(contact.getId()) });

        // close data base
        close();
    }

    public void delete(Contact contact) {
        // open data base
        open();

        // exécute le delete avec la clause where id = ?
        db.delete(ContactDBHelper.CONTACT_TABLE_NAME,ContactDBHelper.CONTACT_KEY + " = ?",
                new String[] { String.valueOf(contact.getId()) });

        // close data base
        close();
    }
}
