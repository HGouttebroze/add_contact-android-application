package com.hb.cda.hugues.contactpro;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.hb.cda.hugues.contactpro.DAO.ContactDAO;
import com.hb.cda.hugues.contactpro.adapters.ContactAdapter;
import com.hb.cda.hugues.contactpro.pojos.Contact;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ContactDAO contactDAO;
    private List<Contact> contacts;
    private Context context;
    private TextView tvContact;
    private RecyclerView rvContact;
    private ContactAdapter contactAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recyclerview_main);
        /*
        setContentView(R.layout.activity_main);

        // check elements
        tvContact = findViewById(R.id.tvContact);
         */
        // check elements
        rvContact = findViewById(R.id.rvContact);
        // check context
        context = getApplicationContext();
        //create DAO
        contactDAO = new ContactDAO(getApplicationContext());
        // create layoutManager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        rvContact.setHasFixedSize(true);

        rvContact.setLayoutManager(layoutManager);

    }

    @Override
    protected void onStart() {
        super.onStart();

        // Call method that send Async
        ContactAsyncTasks contactAsyncTasks = new ContactAsyncTasks();
        contactAsyncTasks.execute();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // create menu
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_contact:
                // call & start activity "AddContactActivity with an intent
                Intent intent = new Intent(getApplicationContext(), AddContactActivity.class);

                // start activity
                startActivity(intent);

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState, @NonNull PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }

    public class ContactAsyncTasks extends AsyncTask<String, String, List<Contact>> {

        @Override
        protected List<Contact> doInBackground(String... strings) {

            List<Contact> responseContact = new ArrayList<>();

            try {
                responseContact = contactDAO.list();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            //return null;
            return responseContact;
        }

        @Override
        protected void onPostExecute(List<Contact> contacts) {
            //super.onPostExecute(contacts);
            contactAdapter = new ContactAdapter(contacts);
            rvContact.setAdapter(contactAdapter);
        }
    }
}