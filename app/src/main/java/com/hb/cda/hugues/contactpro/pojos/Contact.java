package com.hb.cda.hugues.contactpro.pojos;

import android.os.Parcel;
import android.os.Parcelable;

public class Contact implements Parcelable {

    private long id;
    private String nom;
    private String prenom;
    private String societe;
    private String adresse;
    private String tel;
    private String adresseMail;
    private String siteWeb;
    private String secteur;
    private int favori;


    protected Contact(Parcel in) {
        id = in.readLong();
        nom = in.readString();
        prenom = in.readString();
        societe = in.readString();
        adresse = in.readString();
        tel = in.readString();
        adresseMail = in.readString();
        siteWeb = in.readString();
        secteur = in.readString();
        favori = in.readInt();
    }

    public Contact() {
    }

    public Contact(long id, String nom, String prenom, String societe, String adresse, String tel, String adresseMail, String siteWeb, String secteur, int favori) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.societe = societe;
        this.adresse = adresse;
        this.tel = tel;
        this.adresseMail = adresseMail;
        this.siteWeb = siteWeb;
        this.secteur = secteur;
        this.favori = favori;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getSociete() {
        return societe;
    }

    public void setSociete(String societe) {
        this.societe = societe;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getAdresseMail() {
        return adresseMail;
    }

    public void setAdresseMail(String adresseMail) {
        this.adresseMail = adresseMail;
    }

    public String getSiteWeb() {
        return siteWeb;
    }

    public void setSiteWeb(String siteWeb) {
        this.siteWeb = siteWeb;
    }

    public String getSecteur() {
        return secteur;
    }

    public void setSecteur(String secteur) {
        this.secteur = secteur;
    }

    public int getFavori() {
        return favori;
    }

    public void setFavori(int favori) {
        this.favori = favori;
    }

    public static Creator<Contact> getCREATOR() {
        return CREATOR;
    }

    public static final Creator<Contact> CREATOR = new Creator<Contact>() {
        @Override
        public Contact createFromParcel(Parcel in) {
            return new Contact(in);
        }

        @Override
        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(nom);
        dest.writeString(prenom);
        dest.writeString(societe);
        dest.writeString(adresse);
        dest.writeString(tel);
        dest.writeString(adresseMail);
        dest.writeString(siteWeb);
        dest.writeString(secteur);
        dest.writeInt(favori);
    }

    @Override
    public String toString() {
        return "Contact{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", societe='" + societe + '\'' +
                ", adresse='" + adresse + '\'' +
                ", tel='" + tel + '\'' +
                ", adresseMail='" + adresseMail + '\'' +
                ", siteWeb='" + siteWeb + '\'' +
                ", secteur='" + secteur + '\'' +
                ", favori=" + favori +
                '}';
    }
}
