package com.hb.cda.hugues.contactpro;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.hb.cda.hugues.contactpro.DAO.ContactDAO;
import com.hb.cda.hugues.contactpro.pojos.Contact;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AddContactActivity extends AppCompatActivity {

    // vue elements variable
    private Spinner sprSecteur;
    private EditText etPrenon;
    private EditText etNom;
    private EditText etAdresse;
    private EditText etSiteWeb;
    private EditText etTel;
    private EditText etSociete;
    private EditText etAdresseMail;
    private Button btnAdd;
    private Button btnCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);

        // to go back in action bar
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);

        // check vue elements
        btnAdd = findViewById(R.id.btnAdd);
        btnCancel = findViewById(R.id.btnCancel);
        etNom = findViewById(R.id.etNom);
        etPrenon = findViewById(R.id.etPrenom);
        etSociete = findViewById(R.id.etSociete);
        etAdresse = findViewById(R.id.etAdresse);
        etAdresseMail = findViewById(R.id.etAdresseMail);
        etSiteWeb = findViewById(R.id.etSiteWeb);
        etTel =findViewById(R.id.etTel);
        sprSecteur = findViewById(R.id.sprSecteur);

        // create an list in a string's array that spinner will using
        // créé la liste qui sera affichée par le spinner
        String[] urgency = new String[]{
                "Industrie",
                "Informatique",
                "Santé",
                "Recherche",
                "BTP",
                "Artisanat",
                "Spectacle",
                "Horticulture et Maraîchage",
                "Enseignement",
                "Services",
        };
        final List<String> urgencyList = new ArrayList<>(Arrays.asList(urgency));

        // créé l'adpater pour le spinner
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_secteur, urgencyList
        );

        // ajoute l'adapter au spinner
        sprSecteur.setAdapter(spinnerArrayAdapter);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String prenom = etPrenon.getText().toString();
                String nom = etNom.getText().toString();
                String societe = etSociete.getText().toString();
                String adresse = etAdresse.getText().toString();
                String tel = etTel.getText().toString();
                String adresseMail = etAdresseMail.getText().toString();
                String siteWeb = etSiteWeb.getText().toString();
                String selectedSector = sprSecteur.getSelectedItem().toString();

                // nom, prenom, soci, adre, tel
                if (nom.isEmpty()){
                    Toast toast = Toast.makeText(getApplicationContext(), "Merci de renseigner un nom.", Toast.LENGTH_SHORT);
                    toast.show();
                } else if (prenom.isEmpty()) {
                    Toast toast = Toast.makeText(getApplicationContext(), "Merci de renseigner un prénom", Toast.LENGTH_SHORT);
                    toast.show();
                }
                else if (societe.isEmpty()) {
                    Toast toast = Toast.makeText(getApplicationContext(), "Merci de renseigner une société.", Toast.LENGTH_SHORT);
                    toast.show();
                }
                else if (adresse.isEmpty()) {
                    Toast toast = Toast.makeText(getApplicationContext(), "Merci de renseigner une adresse.", Toast.LENGTH_SHORT);
                    toast.show();
                }
                else if (tel.isEmpty()) {
                    Toast toast = Toast.makeText(getApplicationContext(), "Merci de renseigner un numéro de téléphone.", Toast.LENGTH_SHORT);
                    toast.show();
                }

                else {

                    // record in data base with DAO
                    Contact contact = new Contact();
                    contact.setPrenom(prenom);
                    contact.setNom(nom);
                    contact.setSociete(societe);
                    contact.setAdresse(adresse);
                    contact.setTel(tel);
                    contact.setAdresseMail(adresseMail);
                    contact.setSiteWeb(siteWeb);
                    contact.setSecteur(selectedSector);

                    ContactDAO contactDAO = new ContactDAO(getApplicationContext());
                    contactDAO.add(contact);

                    // termine l'activity et revient à la main
                    finish();
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // finish activity & come back to main
                finish();
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
  /*
    if (name.isEmpty()){
        Toast toast = Toast.makeText(getApplicationContext(), "Enter a name", Toast.LENGTH_SHORT);
        toast.show();
    }
            else if (company.isEmpty()) {
        Toast toast = Toast.makeText(getApplicationContext(), "Enter a company", Toast.LENGTH_SHORT);
        toast.show();
    }
            else if (adress.isEmpty()) {
        Toast toast = Toast.makeText(getApplicationContext(), "Enter an adress", Toast.LENGTH_SHORT);
        toast.show();
    }
            else if (telNumber.isEmpty()) {
        Toast toast = Toast.makeText(getApplicationContext(), "Enter a telephone number", Toast.LENGTH_SHORT);
        toast.show();
    }
            else if (email.isEmpty()) {
        Toast toast = Toast.makeText(getApplicationContext(), "Enter an email", Toast.LENGTH_SHORT);
        toast.show();
    }
            else if (website.isEmpty()) {
        Toast toast = Toast.makeText(getApplicationContext(), "Enter a website", Toast.LENGTH_SHORT);
        toast.show();
    }
            else if (selectedActivityArea.isEmpty()) {
        Toast toast = Toast.makeText(getApplicationContext(), "Select an activity area", Toast.LENGTH_SHORT);
        toast.show();
    }

   */
}