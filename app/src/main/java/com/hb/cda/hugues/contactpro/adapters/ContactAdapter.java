package com.hb.cda.hugues.contactpro.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hb.cda.hugues.contactpro.R;
import com.hb.cda.hugues.contactpro.pojos.Contact;

import java.util.List;


public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactViewHolder> {

    private List<Contact> contacts;

    public class ContactViewHolder extends RecyclerView.ViewHolder{

        public TextView tvNom;
        public TextView tvPrenom;
        public TextView tvSociete;

        public ContactViewHolder(@NonNull View itemView) {
            super(itemView);

            // check vue elements
            tvNom = itemView.findViewById(R.id.tvNom);
            tvPrenom = itemView.findViewById(R.id.tvPrenom);
            tvSociete = itemView.findViewById(R.id.tvSociete);
        }
    }

    // add constructor for contacts list
    public ContactAdapter(List<Contact> contacts) {
        this.contacts = contacts;
    }

    // create item view
    @NonNull
    @Override
    public ContactAdapter.ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_item, parent,false);

        return new ContactViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactAdapter.ContactViewHolder holder, int position) {
        // check contact object
        Contact contact = contacts.get(position);

        holder.tvNom.setText(contact.getNom());
        holder.tvPrenom.setText(contact.getPrenom());
        holder.tvSociete.setText(contact.getSociete());
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

}
